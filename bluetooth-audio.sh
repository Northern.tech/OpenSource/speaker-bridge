apt-get -qqy install bluez-tools pulseaudio pulseaudio-module-bluetooth

usermod -a -G lp pi
newgrp lp
usermod -a -G bluetooth pulse

sudo su

sed -i "s/#Class = 0x000100/Class = 0x41C/" /etc/bluetooth/main.conf
sed -i "s/#DiscoverableTimeout = 0/DiscoverableTimeout = 0/" /etc/bluetooth/main.conf

cat >/etc/systemd/system/pulseaudio.service <<EOF
[Unit]
Description=PulseAudio Daemon

[Service]
Type=simple
PrivateTmp=true
ExecStart=/usr/bin/pulseaudio --system --realtime --disallow-exit --no-cpu-limit

[Install]
WantedBy=multi-user.target
EOF

cat > /etc/pulse/system.pa << EOF
### Automatically load driver modules for Bluetooth hardware
.ifexists module-bluetooth-policy.so
load-module module-bluetooth-policy
.endif
.ifexists module-bluetooth-discover.so
load-module module-bluetooth-discover
.endif
EOF

cat >/etc/bluetooth/pin.conf <<EOF
*	0000
EOF
chmod 600 /etc/bluetooth/pin.conf

cat >/etc/systemd/system/bt-agent.service <<EOF
[Unit]
Description=Bluetooth Auth Agent
After=bluetooth.service
PartOf=bluetooth.service

[Service]
Type=simple
ExecStart=/usr/bin/bt-agent -c NoInputNoOutput -p /etc/bluetooth/pin.conf
ExecStartPost=/bin/sleep 1
ExecStartPost=/bin/hciconfig hci0 sspmode 0

[Install]
WantedBy=bluetooth.target
EOF

systemctl enable pulseaudio
systemctl start pulseaudio
systemctl enable bt-agent
systemctl start bt-agent
systemctl restart bluetooth
systemctl daemon-reload
systemctl restart bt-agent
reboot