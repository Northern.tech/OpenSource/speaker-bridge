curl -sL https://dtcooper.github.io/raspotify/install.sh | sh
cat >/etc/default/raspotify <<EOF
  # Device name on Spotify Connect
  DEVICE_NAME="MenderingSpeaker"
  # Bitrate, one of 96 (low quality), 160 (default quality), or 320 (high quality)
  BITRATE="320"

  # To make your device visible on Spotify Connect across the Internet add your
  # username and password which can be set via "Set device password", on your
  # account settings, use `--username` and `--password`.
  # OPTIONS="--username <username> --password <password>"
EOF

cat >/home/pi/watchdog.sh <<'EOF'
#!/bin/sh
status=$(systemctl status raspotify | grep -E "(WARN|ERROR|101)")
if [ ! -z "$status" ]; then
  systemctl restart raspotify
fi
EOF
sudo su
chmod +x /home/pi/watchdog.sh
cat > /etc/crontab << EOF
* * * * * root /home/pi/watchdog.sh
EOF
